// (c) 2012 Braydon Fuller. Some rights reserved.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

float pad4;
float pad3;
float pad7;
float pad8;
float pad5;
float pad6;
float pad1;
float pad2;


// impulse to filter to dac
Impulse i => BiQuad f => Gain out => Gain g => dac;
//adc => Impulse i => BiQuad f => Gain out => Gain g => dac;

out => Delay delay => out;

SinOsc s => g;
3 => g.op;
1000.0 => s.freq;

// our radius
.99999 => float R;
// our delay order
500 => float L;
// set delay
L::samp => delay.delay;
// set dissipation factor
Math.pow( R, L ) => delay.gain;

// set the filter's pole radius
.99 => f.prad; 
// set equal gain zeros
1 => f.eqzs;
// set filter gain
.9 => f.gain;
  
// devices to open (try: chuck --probe)
MidiIn min[16];

// number of devices
int devices;

// loop
for( int i; i < min.cap(); i++ )
{
    // no print err
    min[i].printerr( 0 );

    // open the device
    if( min[i].open( i ) )
    {
        <<< "device", i, "->", min[i].name(), "->", "open: SUCCESS" >>>;
        spork ~ go( min[i], i );
        devices++;
    }
    else break;
}

// check
if( devices == 0 )
{
    <<< "um, couldn't open a single MIDI device, bailing out..." >>>;
    me.exit();
}


// time loop
while( true )
{
    100::ms => now;
}

// handler for one midi event
fun void go( MidiIn min, int id )
{
    // the message
    MidiMsg msg;

    // infinite event loop
    while( true )
    {
        // wait on event
        min => now;

        // print message
        while( min.recv( msg ) )
        {

			if ( msg.data1 == 176 ) 
			{
				float pfreq1;

				if ( msg.data2 == 1 ) {
					float v1;
					msg.data3 => v1;
					v1 / 127 => pfreq1;
				    Std.fabs(Math.sin(pfreq1)) * 8000.0 => pad8;
				    Std.fabs(Math.sin(pfreq1)) * 7000.0 => pad7;
				    Std.fabs(Math.sin(pfreq1)) * 6000.0 => pad6;
				    Std.fabs(Math.sin(pfreq1)) * 4000.0 => pad5;
				    Std.fabs(Math.sin(pfreq1)) * 2000.0 => pad4;
				    Std.fabs(Math.sin(pfreq1)) * 1000.0 => pad3;
				    Std.fabs(Math.sin(pfreq1)) * 500.0 => pad2;
				    Std.fabs(Math.sin(pfreq1)) * 100.0 => pad1;
				}

				if ( msg.data2 == 2 ) {
					msg.data3 => float d1;
					127 / d1 => float d1p;
					// our delay order
					(500 * d1p) + 1 => float L;
					// set delay
					L::samp => delay.delay;
					Math.pow( R, L ) => delay.gain;
				}

				if ( msg.data2 == 3 ) {
					msg.data3 => float p1;
					0.69 + p1 / 127 * 0.3 => float prad1;
					if ( prad1 == 1 ) 
					{
						.99 => prad1;
					}
					<<< "prad1", prad1 >>>;
					prad1 => f.prad; 
				}

				if ( msg.data2 == 4 ) {
					msg.data3 => float rf;
					rf / 127 * 5000 => float rfreq;
					<<< "rfreq", rfreq >>>;
					rfreq => s.freq; 
				}
			}

			float g1;

			if ( msg.data1 == 128 ) {
			}

			if ( msg.data1 == 144 ) {
				if ( msg.data2 == 40 ) {
					<<< "pad5", pad5 >>>;
					pad5 => f.pfreq;
					msg.data3 => g1;
					g1 / 127 * 2 => f.gain;
				    1.0 => i.next;
				}
				if ( msg.data2 == 41 ) {
					<<< "pad6", pad6 >>>;
					pad6 => f.pfreq;
					msg.data3 => g1;
					g1 / 127 * 2=> f.gain;
				    1.0 => i.next;
				}
				if ( msg.data2 == 42 ) {
					<<< "pad7", pad7 >>>;
					pad7 => f.pfreq;
					msg.data3 => g1;
					g1 / 127 * 2=> f.gain;
				    1.0 => i.next;
				}
				if ( msg.data2 == 43 ) {
					<<< "pad8", pad8 >>>;
					pad8 => f.pfreq;
					msg.data3 => g1;
					g1 / 127 * 2=> f.gain;
				    1.0 => i.next;
				}
				if ( msg.data2 == 36 ) {
					<<< "pad1", pad1 >>>;
					pad1 => f.pfreq;
					msg.data3 => g1;
					g1 / 127 * 2 => f.gain;
				    1.0 => i.next;
				}
				if ( msg.data2 == 37 ) {
					<<< "pad1", pad2 >>>;
					pad2 => f.pfreq;
					msg.data3 => g1;
					g1 / 127 * 2 => f.gain;
				    1.0 => i.next;
				}
				if ( msg.data2 == 38 ) {
					<<< "pad3", pad3 >>>;
					pad3 => f.pfreq;
					msg.data3 => g1;
					g1 / 127 * 2 => f.gain;
				    1.0 => i.next;
				}
				if ( msg.data2 == 39 ) {
					<<< "pad4", pad4 >>>;
					pad4 => f.pfreq;
					msg.data3 => g1;
					g1 / 127 * 2 => f.gain;
				    1.0 => i.next;
				}
			} 

            // print out midi message with id
            <<< "device", id, ":", msg.data1, msg.data2, msg.data3 >>>;
        }
    }
}
