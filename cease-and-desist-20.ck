// (c) 2012 Braydon Fuller. Some rights reserved.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

"bonus-beats-blast-anenon-embers_and_ashes-cc-by-dublab.flac" => string filename;
"voicetrack1.flac" => string filename2;
"voicetrack2.flac" => string filename3;
"voicetrack3.flac" => string filename4;

// our patch - feedforward part
SndBuf buf => Gain d => dac;
Gain g2 => Chorus c => dac;

SndBuf buf2 => g2;
SndBuf buf3 => g2;

filename => buf.read;
filename2 => buf2.read;
filename3 => buf3.read;

0 => buf.gain;
0 => buf.rate;
0 => buf2.gain;
0 => buf2.rate;
0 => buf3.gain;
0 => buf3.rate;

0.07 => c.modFreq;
0 => c.modDepth;
0.4 => c.mix;

g2 => Echo e3 => g2;

// feedback
d => Gain g3 => Echo d2 => d;

30::ms => d2.delay;

// set parameters
30::ms => d2.delay;
1 => d.gain;
0.7 => g2.gain;
0.9 => g3.gain;

// opens MIDI input devices one by one, starting from 0,
// until it reaches one it can't open.  then waits for
// midi events on all open devices and prints out the
// device, and contents of the MIDI message

// devices to open (try: chuck --probe)
MidiIn min[16];

// number of devices
int devices;

// loop
for( int i; i < min.cap(); i++ )
{
    // no print err
    min[i].printerr( 0 );

    // open the device
    if( min[i].open( i ) )
    {
        <<< "device", i, "->", min[i].name(), "->", "open: SUCCESS" >>>;
        spork ~ go( min[i], i );
        devices++;
    }
    else break;
}

// check
if( devices == 0 )
{
    <<< "um, couldn't open a single MIDI device, bailing out..." >>>;
    me.exit();
}


// time loop
while( true )
{
    100::ms => now;
}

// infinite time loop
//while( true ) 1::second => now;

// handler for one midi event
fun void go( MidiIn min, int id )
{
    // the message
    MidiMsg msg;

	float rate2;
	float gain2;
	1 => rate2;
	1 => gain2;

    // infinite event loop
    while( true )
    {
        // wait on event
        min => now;

        // print message
        while( min.recv( msg ) )
        {
			if ( msg.data1 == 176 || msg.data1 == 177 ) 
			{
				float rate1;
				float gain1;

				if ( msg.data2 == 1 ) {
					float g1;
					msg.data3 => g1;
					g1 / 127 * 2 => gain1;
					<<< "gain1", gain1 >>>;
					gain1 => buf.gain;
				}

				if ( msg.data2 == 2 ) {
					float r1;
					msg.data3 => r1;
					r1 / 127 * 2 => rate1;
					<<< "rate1", rate1 >>>;
					rate1 => buf.rate;
				}

				if ( msg.data2 == 3 ) {
					float g2;
					msg.data3 => g2;
					g2 / 127 * 4 => gain2;
					<<< "gain2", gain2 >>>;
					if ( msg.data1 == 176 ) {
						gain2 => buf2.gain;
					}
					if ( msg.data1 == 177 ) {
						gain2 => buf3.gain;
					}
				}

				if ( msg.data2 == 4 ) {
					float r2;
					msg.data3 => r2;
					r2 / 127 * 2 => rate2;
					<<< "rate4", rate2 >>>;
					if ( msg.data1 == 176 ) {
						rate2 => buf2.rate;
					}
					if ( msg.data1 == 177 ) {
						rate2 => buf3.rate;
					}
				}

				if ( msg.data2 == 5 ) {
					msg.data3 => float dd1;
					dd1/127 => float delay1;
					1000.0 * delay1 => float delay2;
					1000::ms => d2.max;
					<<< "delay2", delay2 >>>;
					delay2::ms => d2.delay;
				}

				if ( msg.data2 == 6 ) {
					msg.data3 => float dd2;
					dd2/127 => float delay2;
					1000.0 * delay2 => float delay3;
					1000::ms => e3.max;
					<<< "delay3", delay3 >>>;
					delay3::ms => e3.delay;
				}


				if ( msg.data2 == 7 ) {
					msg.data3 => float scratch;
				}

				if ( msg.data2 == 8 ) {
					msg.data3 => float scratchfriction;
				}


			}

			if ( msg.data1 == 145 ) {

				if ( msg.data2 == 35 || 
					msg.data2 == 36 || 
					msg.data2 == 42 || 
					msg.data2 == 39 ) {
					0 => buf2.rate;
					rate2 => buf3.rate;
					gain2 => buf3.gain;
				}

				if ( msg.data2 == 35 ) {
					0 => buf3.pos;
				}
				if ( msg.data2 == 36 ) {
					150000 => buf3.pos;
				}
				if ( msg.data2 == 42 ) {
					360000 => buf3.pos;
				}
				if ( msg.data2 == 39 ) {
					567000 => buf3.pos;
				}
			}

			if ( msg.data1 == 144 ) {

				if ( msg.data2 == 40 ) {
					0 => buf.pos;
				}
				if ( msg.data2 == 41 ) {
					620000 => buf.pos;
				}
				if ( msg.data2 == 42 ) {
					700000 => buf.pos;
				}
				if ( msg.data2 == 43 ) {
					4500000 => buf.pos;
				}

			}

			if ( msg.data1 == 145 ) {

				if ( msg.data2 == 37 ) {
					0 => buf.pos;
				}
				if ( msg.data2 == 38 ) {
					620000 => buf.pos;
				}
				if ( msg.data2 == 46 ) {
					700000 => buf.pos;
				}
				if ( msg.data2 == 44 ) {
					4500000 => buf.pos;
				}

			}

			if ( msg.data1 == 144 ) {
				if ( msg.data2 == 36 ||
					msg.data2 == 37 ||
					msg.data2 == 38 ||
					msg.data3 == 39 ) {

					0 => buf3.rate;
					rate2 => buf2.rate;
					gain2 => buf2.gain;
				}

				if ( msg.data2 == 36 ) {
					393000 => buf2.pos;
				}
				if ( msg.data2 == 37 ) {
					2700000 => buf2.pos;
				}
				if ( msg.data2 == 38 ) {
					9822083 => buf2.pos;
				}
				if ( msg.data2 == 39 ) {
					11923830 => buf2.pos;
				}
			} 

            // print out midi message with id
            <<< "device", id, ":", msg.data1, msg.data2, msg.data3 >>>;
        }
    }
}


